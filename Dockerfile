FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2 as convco

COPY convco /convco
RUN mv "/convco/target/$(uname -m)-unknown-linux-musl/release/convco" "/usr/local/bin/convco"

# ---

FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

COPY --from=convco /usr/local/bin/convco /usr/local/bin/convco

RUN apk add --no-cache "git" "git-lfs" "openssh" "nodejs" "yarn" "jq"

COPY . /semantic-release
WORKDIR /semantic-release
RUN yarn

ENV PATH="${PATH}:/semantic-release/node_modules/.bin"
ENV GIT_AUTHOR_NAME="semantic-release-bot"
ENV GIT_AUTHOR_EMAIL="no-reply@semantic-release.bot"
ENV GIT_COMMITTER_NAME="semantic-release-bot"
ENV GIT_COMMITTER_EMAIL="no-reply@semantic-release.bot"

VOLUME /app
WORKDIR /app
