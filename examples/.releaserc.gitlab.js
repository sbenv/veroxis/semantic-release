module.exports = {
    branches: ['*'],
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/changelog",
        ['@semantic-release/git', {
            assets: ['CHANGELOG.md'],
        }],
        ['@semantic-release/gitlab', {
            "assets": [{
                "label": "some_file_title",
                "path": "some_file.tar.gz"
            }]
        }]
    ],
};
