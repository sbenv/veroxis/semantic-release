#!/bin/sh

sed '/convco\/convco/!d' version_manifest.txt | tail -n 1 | cut -d' ' -f5 | cut -d'=' -f2 | cut -d'v' -f2
