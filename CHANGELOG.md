# [1.5.0](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.4.0...v1.5.0) (2022-09-13)


### Features

* remove semantic-release-meta ([ba40c2e](https://gitlab.com/sbenv/veroxis/semantic-release/commit/ba40c2ef5e31d9bd1a9b9538b14321b1eb270225))

# [1.4.0](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.3.5...v1.4.0) (2022-07-13)


### Features

* use alpine image from gitlab ([f3d9fec](https://gitlab.com/sbenv/veroxis/semantic-release/commit/f3d9fecfa983b9e6bb5cd002c4542ba69de5b06a))

## [1.3.5](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.3.4...v1.3.5) (2022-06-15)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.5 ([312f450](https://gitlab.com/sbenv/veroxis/semantic-release/commit/312f450ee6936e6c4dcdc20d076e698d627b67be))

## [1.3.4](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.3.3...v1.3.4) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.4 ([3f1a627](https://gitlab.com/sbenv/veroxis/semantic-release/commit/3f1a627e248f9856be79f8f7e8d537c74336c756))

## [1.3.3](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.3.2...v1.3.3) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.3 ([5887e6c](https://gitlab.com/sbenv/veroxis/semantic-release/commit/5887e6c36aa85b544d5c5a99160c8c9232db5a3f))

## [1.3.2](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.3.1...v1.3.2) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.2 ([a531c9b](https://gitlab.com/sbenv/veroxis/semantic-release/commit/a531c9bbab3e5e332da940bea57deb0fe52e237e))

## [1.3.1](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.3.0...v1.3.1) (2022-06-13)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.1 ([7818c1a](https://gitlab.com/sbenv/veroxis/semantic-release/commit/7818c1a65c45b8b67c161d59dc4093bfe743e64c))

# [1.3.0](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.2.0...v1.3.0) (2022-05-29)


### Features

* install convco ([1e373f3](https://gitlab.com/sbenv/veroxis/semantic-release/commit/1e373f39aef9992bb496b67288aecb29665cc838))

# [1.2.0](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.1.0...v1.2.0) (2022-05-29)


### Features

* make `--no-ci` optional ([d689d3a](https://gitlab.com/sbenv/veroxis/semantic-release/commit/d689d3a477a7757d45f5ea723065d40cf6690212))

# [1.1.0](https://gitlab.com/sbenv/veroxis/semantic-release/compare/v1.0.0...v1.1.0) (2022-05-29)


### Features

* add `git-lfs` ([04fc99f](https://gitlab.com/sbenv/veroxis/semantic-release/commit/04fc99fabb7f89976bf8156adbe7a471c18cdc8e))

# 1.0.0 (2022-05-21)


### Bug Fixes

* set correct plugin name for git in releaserc files ([9ef1639](https://gitlab.com/sbenv/veroxis/semantic-release/commit/9ef163911e0a2d93bf3231c4dacf2ea15f1064ae))


### Features

* allow outputting version to files ([94ce4b9](https://gitlab.com/sbenv/veroxis/semantic-release/commit/94ce4b996043eaf9b2cd53ceeade9c37fc63ee8d))
* create semantic-release-meta application ([c1fd8d7](https://gitlab.com/sbenv/veroxis/semantic-release/commit/c1fd8d7c24fb11f968fe4ecd39e2eded2d6fc77d))
* initial commit ([f6ce4f9](https://gitlab.com/sbenv/veroxis/semantic-release/commit/f6ce4f9901eaf8680d7d1fa07ec6e6ea97f0f02e))
